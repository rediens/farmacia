package Logica;

import Persistencia.ConexionBD;
import static java.lang.Math.round;
import java.sql.*;
import java.util.*;

public class Producto {
    private String id;
    private String nombre;
    private double temperatura;
    private double valorBase;
    
    public Producto()
    {
        
    }
    
    public Producto(String id, String nombre, double temperatura, double valorBase)
    {
        this.id = id;
        this.nombre = nombre;
        this.temperatura = temperatura;
        this.valorBase = valorBase;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getValorBase() {
        return valorBase;
    }

    public void setValorBase(double valorBase) {
        this.valorBase = valorBase;
    }
    public double calcularCosto()
    {
        if(this.temperatura > 20)
        {
            return round(valorBase * 110 / 100);
        }
        else
        {
             return round(valorBase * 120 / 100);
        }
    }

    @Override
    public String toString() {
        return "Producto{" + "id=" + id + ", nombre=" + nombre + ", temperatura=" + temperatura + ", valorBase=" + valorBase + '}';
    }
    public boolean guardarProducto()
    {
        ConexionBD conexion = new ConexionBD();
        String sql = "INSERT INTO productos (id,nombre,temperatura,valorBase)"
                + "VALUES('" + this.id + "','" + this.nombre + "','" + this.temperatura + "','" + this.valorBase +"')";
        if(conexion.setAutoCommitBD(false))
        {
            if(conexion.insertarBD(sql))
            {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            }
            else
            {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
            
        }
        else
        {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public boolean actualizarProductos()
    {
        ConexionBD conexion = new ConexionBD();
        String sql = "UPDATE productos SET valorBase='" + this.valorBase + "',nombre='" + this.nombre 
                + "',temperatura='" + this.temperatura + "' WHERE id=" + this.id;
        if (conexion.setAutoCommitBD(false)) 
        {
            if (conexion.actualizarBD(sql)) 
            {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } 
            else 
            {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;   
            }
        } 
        else 
        {
            conexion.cerrarConexion();
            return false;
        }
                
    }
    
    public boolean eliminarProducto()
    {
        ConexionBD conexion = new ConexionBD();
        String sql = "DELETE FROM productos WHERE id=" + this.id;
        if (conexion.setAutoCommitBD(false)) 
        {
            if (conexion.actualizarBD(sql)) 
            {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } 
            else 
            {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } 
        else 
        {
            conexion.cerrarConexion();
            return false;
        }
    }
    
    public List<Producto> listarProductos()
    {
        List<Producto> listaProductos = new ArrayList<>();
        ConexionBD conexion = new ConexionBD();
        String sql = "SELECT * FROM productos;";
        try 
        {
            ResultSet rs = conexion.consultarBD(sql);
            Producto p;
            while(rs.next())
            {
                p = new Producto();
                p.setId(rs.getString("id"));
                p.setNombre(rs.getString("nombre"));
                p.setTemperatura(rs.getDouble("temperatura"));
                p.setValorBase(rs.getDouble("valorBase"));
                listaProductos.add(p);
            }
        }catch (SQLException ex) {
            System.out.println("Error al listar productos:" + ex.getMessage());
        }finally {
            conexion.cerrarConexion();
        }
        return listaProductos;
        
    }
    
}
